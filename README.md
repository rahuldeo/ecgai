README


This README would normally document whatever steps are necessary to get your application up and running.
What is this repository for?

This repository includes source code and training data for implementation of a model for segmentation of electrocardiograms.

How do I get set up?
Dependencies: 

1. tensorflow 
2. hmmlearn 
3. easydict 
4. scipy 
5. numpy

Who do I talk to?
rahul.c.deo at gmail.com

The model is stored in the following Dropbox directory:

https://www.dropbox.com/sh/eb2krfuz8xt87az/AADKsDhxQzQrV40Pwx7gMTQGa?dl=0

To run just type python ecgHMM.py


