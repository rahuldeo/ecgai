from __future__ import print_function, division

import numpy as np
import tensorflow as tf

import sys, os, time, json
from easydict import EasyDict as edict

from util import *

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

flags = tf.app.flags

# optional arguments
flags.DEFINE_boolean('train', True, 'True for training, False for testing phase. Default [True]')
flags.DEFINE_string('gpu', '0', 'GPU number. Default [0]')
flags.DEFINE_string('new_config', None, 'New config directory to make to store new json with overrides')
flags.DEFINE_string('save_root', '/media/deoraid03/rdeo/', 'Checkpoints will be saved in subdirectories of this root. Symlinks will point to train subdirectories. Default: ["/media/deoraid03/rdeo/"]')
flags.DEFINE_string('study_num', '0', 'Cross validation study split, Default: 0')
flags.DEFINE_boolean('debug', False, 'If true, train and validate for 1 iteration.')
flags.DEFINE_boolean('retrain', False, 'If true, trains a new model and will override old models.')

FLAGS = flags.FLAGS
        
def main(argv):
    assert len(argv) == 2, 'Only argument must be path to config directory'
    config_dir = os.path.abspath(argv[1])
    assert config_dir.startswith(Models), 'Invalid config directory %s' % config_dir
    model_name, config_name = config_dir[len(Models):].split('/')[:2]

    os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu
    model_dir = os.path.join(Models, model_name)
    sys.path.append(model_dir)


    # load in model specific configurations
    config_dir = os.path.join(model_dir, config_name)
    config_path = os.path.join(config_dir, 'config.json')
    with open(config_path, 'r+') as f:
        config = edict(json.load(f))

    data_dir = os.path.join(Data, config.data)
    sys.path.append(data_dir)
    
    # check for existing checkpoints
    if FLAGS.save_root:
        for dname in ['train', 'val']:
            orig_dir = os.path.join(config_dir, dname)
            save_dir = os.path.join(FLAGS.save_root, 'ecg','models', model_name, config_name, dname)
            make_dir(save_dir)

            if not os.path.exists(orig_dir):
                os.symlink(save_dir, orig_dir)
                print('Creating symlink %s -> %s' % (orig_dir, save_dir))
            elif not os.path.islink(orig_dir):
                raise RuntimeError('%s exists but is not a link. Cannot create new link to %s' % (orig_dir, save_dir))
    
    if FLAGS.debug:
        config.disp_interval = config.summary_interval = config.save_interval = config.max_steps = 1

    train_dir = os.path.join(config_dir, 'train', FLAGS.study_num)
    
    ckpt = tf.train.get_checkpoint_state(train_dir)
    if ckpt and not FLAGS.retrain:
        print('Latest checkpoint:', ckpt.model_checkpoint_path)
    elif not FLAGS.train:
        raise RuntimeError('Cannot find checkpoint to test from, exiting')

    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
    
    # load model with the given configurations
    from load_model_rcd import load_model, load_data

    model = load_model(config, sess)
    model.init_weights()
    x_train, x_test, y_train, y_test = load_data(config, FLAGS.study_num)

    # create saver and load in data 

    saver = tf.train.Saver(tf.global_variables(), max_to_keep=None)

    # initialize model
    if ckpt and not FLAGS.retrain:
        saver.restore(sess, ckpt.model_checkpoint_path)
    else:
        sess.run(tf.global_variables_initializer())

    val_output_dir = os.path.join(config_dir, 'val', FLAGS.study_num, 'output')

    if FLAGS.train:
        # train model
        summary_writer = tf.summary.FileWriter(train_dir)
        checkpoint_path = os.path.join(train_dir, 'model.ckpt')
        success = model.train(x_train, y_train, x_test, y_test, saver, summary_writer, checkpoint_path, val_output_dir)
        if not success:
            print('Exiting script')
            exit()
    else:
        print('Saving results')
        model.visualize(x_test, y_test, val_output_dir)

if __name__ == '__main__':
    tf.app.run()
